import requests
import re
import time
import os
import webbrowser

url = "https://www.sahibinden.com/kiralik-daire"
headers = { 'Cache-Control':'no-cache','user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'}
open_url = "http://www.sahibinden.com/"
last_id = -1
interval = 30

def get_last_id():
	r = requests.get(url, headers=headers)
	ids = re.findall('(?<=tr data-id\=\").+(?=\")', r.text)
	last_id = ids[0]
	return last_id

	# The notifier function

def notify(title, text):
    os.system("""
              osascript -e 'display notification "{}" with title "{}"'
              """.format(text, title))

notify("sahibinden-checker", "Scan started")
print('sahibinden checker starting...')

i = -1
while True:
	time.sleep(interval)
	current_last_id = get_last_id()
	if current_last_id == last_id:
		i+=1
		msg = "Check:"
		print(msg, i, time.strftime("%H:%M:%S", time.localtime()))
		# os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))
		continue
	
	if i == -1:
		i = 0
		last_id = current_last_id
		continue

	last_id = current_last_id
	open_url = "https://www.sahibinden.com/ilan/"+str(last_id)+"/detay"
	webbrowser.open_new_tab(open_url)
	msg = "New ilan found @" + str(time.strftime("%H:%M:%S", time.localtime()))
	print(msg)
	os.system('afplay /System/Library/Sounds/Sosumi.aiff')
	notify("sahibinden-checker", msg)
	i = 0
